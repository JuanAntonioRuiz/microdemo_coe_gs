package com.example.microdemo.model;

import java.io.Serializable;

public class ModelData implements Serializable {
	
	private int id;
	private int code;
	private boolean status;
	private String message;
	
	public int getId() {
		return code;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
