package com.example.microdemo.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.microdemo.model.ModelData;

@RestController
@RequestMapping(path = "/microDemo")
public class RestData {
	
	//Método GET
	@GetMapping(path = "/metodoGet/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ModelData getData(@PathVariable(name = "id") int id) {
		
		ModelData response = new ModelData();
		if(id == 1) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método GET, registro de ID "+id+": Jazmín Sandoval Hernández");
		} else if( id == 2) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método GET, registro de ID "+id+": Juan Antonio Ruíz González");
		} else if( id == 3) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método GET, registro de ID "+id+": Santiago Nicolás Ruíz");
		} else {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método GET, sin registro con ID "+id);
		}
		return response;
		
	}

	//Método POST
	@PostMapping(path = "/metodoPost", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ModelData getData(@RequestParam(name = "mensaje") String message) {
		
		ModelData response = new ModelData();
		response.setCode(200);
		response.setStatus(true);
		response.setMessage("Acceso exitoso al método POST, mensaje recibido: "+message);
		return response;
	}
	
	//Método PUT	  
	@PutMapping(path = "/metodoPut/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ModelData putData(@PathVariable(name = "id") int id,@RequestParam(name = "mensaje") String message ) {
		
		ModelData response = new ModelData();
		if(id == 1) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método PUT, registro de ID "+id+": Jazmín Sandoval Hernández, actualizado a: "+message);
		} else if( id == 2) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método PUT, registro de ID "+id+": Juan Antonio Ruíz González, actualizado a: "+message);
		} else if( id == 3) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método PUT, registro de ID "+id+": Santiago Nicolás Ruíz, actualizado a: "+message);
		} else {
			response.setCode(402);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método PUT, sin registro con ID "+id+"; no hay nada que actualizar");
		}
		return response;																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																									
	}
	
	//Método DELETE
	@DeleteMapping(path = "/metodoDelete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ModelData deleteData(@PathVariable(name = "id") int id) {
		
		ModelData response = new ModelData();
		if(id == 1) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método DELETE, registro de ID "+id+": Jazmín Sandoval Hernández, eliminado");
		} else if( id == 2) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método DELETE, registro de ID "+id+": Juan Antonio Ruíz González, eliminado");
		} else if( id == 3) {
			response.setCode(200);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método DELETE, registro de ID "+id+": Santiago Nicolás Ruíz, eliminado");
		} else {
			response.setCode(402);
			response.setStatus(true);
			response.setMessage("Acceso exitoso al método DELETE, sin registro con ID "+id+"; no hay nada que eliminar");
		}
		return response;
	}
	
}
